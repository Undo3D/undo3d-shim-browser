//// https://nodejs.org/api/assert.html#assert_assert_doesnotreject_asyncfn_error_message
//// https://github.com/nodejs/node/blob/master/lib/assert.js#L705

//// Load dependencies.
import AssertionError from './assertion-error.mjs'

//// Awaits the asyncFn promise or, if asyncFn is a function, immediately calls
//// the function and awaits the returned promise to complete. It will then
//// check that the promise is not rejected.
//// - asyncFn <Function> | <Promise>
//// - error <RegExp> | <Function>
//// - message <string>
export default async function doesNotReject (fn, ...args) {
    throw Error('@TODO doesNotReject()')
    // expectsNoError(doesNotReject, await waitForActual(fn), ...args)
}


const NO_EXCEPTION_SENTINEL = {}

function expectsNoError (stackStartFn, actual, error, message) {
    if (actual === NO_EXCEPTION_SENTINEL)
        return

    if (typeof error === 'string') {
        message = error
        error = undefined
    }

    if (! error || expectedException(actual, error) ) {
        const details = message ? `: ${message}` : '.'
        const fnType = stackStartFn.name === 'doesNotReject'
          ? 'rejection' : 'exception'
        innerFail({
            actual
          , expected: error
          , operator: stackStartFn.name
          , message: `Got unwanted ${fnType}${details}\n` +
                     `Actual message: "${actual && actual.message}"`
          , stackStartFn
        })
    }
    throw actual
}


function expectedException (actual, expected, msg) {
    if (typeof expected !== 'function') {
        if ( isRegExp(expected) ) //@TODO from require('util').types
            return expected.test(actual)
        // assert.doesNotThrow does not accept objects.
        if (arguments.length === 2) {
            throw new ERR_INVALID_ARG_TYPE(
                'expected', ['Function', 'RegExp'], expected )
        }

        // Handle primitives properly.
        if (typeof actual !== 'object' || actual === null) {
            const err = new AssertionError({
                actual
              , expected
              , message: msg
              , operator: 'deepStrictEqual'
              , stackStartFn: assert.throws
            });
            err.operator = 'throws'
            throw err
        }

        const keys = Object.keys(expected)
        // Special handle errors to make sure the name and the message are
        // compared as well.
        if (expected instanceof Error) {
            keys.push('name', 'message')
        } else if (keys.length === 0) {
            throw new ERR_INVALID_ARG_VALUE('error', expected
              , 'may not be an empty object')
        }
        if (isDeepEqual === undefined) lazyLoadComparison()
        for (const key of keys) {
            if (typeof actual[key] === 'string'
             && isRegExp(expected[key])
             && expected[key].test(actual[key])
            ) {
              continue
            }
            compareExceptionKey(actual, expected, key, msg, keys)
        }
        return true
    }
    // Guard instanceof against arrow functions as they don't have a prototype.
    if (expected.prototype !== undefined && actual instanceof expected) {
        return true
    }
    if (Error.isPrototypeOf(expected)) {
        return false
    }
    return expected.call({}, actual) === true
}


async function waitForActual (promiseFn) {
    let resultPromise
    if (typeof promiseFn === 'function') {
        // Return a rejected promise if `promiseFn` throws synchronously.
        resultPromise = promiseFn()
        // Fail in case no promise is returned.
        if (!checkIsPromise(resultPromise)) {
            throw new ERR_INVALID_RETURN_VALUE(
                'instance of Promise', 'promiseFn', resultPromise)
        }
    } else if (checkIsPromise(promiseFn)) {
        resultPromise = promiseFn
    } else {
        throw new ERR_INVALID_ARG_TYPE(
            'promiseFn', ['Function', 'Promise'], promiseFn)
    }

    try {
        await resultPromise
    } catch (e) {
        return e
    }
    return NO_EXCEPTION_SENTINEL
}
