//// https://nodejs.org/api/assert.html#assert_assert_strictequal_actual_expected_message
//// https://github.com/nodejs/node/blob/master/lib/assert.js

//// Load dependencies.
import { innerFail } from './utility.mjs'

//// Tests strict equality between the `actual` and `expected` arguments as
//// determined by the ‘SameValue Comparison’.
//// - actual <any>
//// - expected <any>
//// - message <string> | <Error>
export default function strictEqual (actual, expected, message) {
    if (! Object.is(actual, expected) ) {
        innerFail({
            actual
          , expected
          , message
          , operator: 'strictEqual'
          , stackStartFn: strictEqual
        })
    }
}
