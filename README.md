# Undo3D Shim Browser

#### A collection of browser shims and polyfills for Undo3D apps

[undo3d.com](https://undo3d.com/)
&nbsp; [Main Repo](https://gitlab.com/Undo3D/undo3d-shim-browser)


#### assert

As close to Node’s ['assert' module](https://nodejs.org/api/assert.html) as possible.  
__Status:__ partially implemented, but still plenty to do.


#### fs

Equivalent to Node’s ['fs' module,](https://nodejs.org/api/fs.html) but based on IndexedDB.  
__Status:__ not yet begun.
